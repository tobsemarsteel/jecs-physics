package com.tmarsteel.jecs.physics.examples

import com.tmarsteel.jecs.BaseEntity
import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.physics.*
import com.tmarsteel.jecs.system.motion.LocationComponent
import com.tmarsteel.jecs.system.motion.VelocityComponent
import com.tmarsteel.jecs.util.Vector
import java.awt.*
import java.util.*
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel

val TICKRATE: Float = 50f // Tickrate in Hz

class ThrownBallFrame : JFrame("ThrownBall at half-speed") {

    private var panel: BallThrowPanel? = null

    init {
        initComponents()
    }

    private fun initComponents() {
        panel = BallThrowPanel()

        layout = BorderLayout()
        contentPane.add(panel, BorderLayout.CENTER)

        // action button
        val doBtn = JButton("Throw!")
        doBtn.addActionListener {
            panel!!.start()
        }
        contentPane.add(doBtn, BorderLayout.NORTH)

        // setup frame
        size = Dimension(550, 600)
        setLocationRelativeTo(null)
    }

    companion object {
        class BallThrowPanel : JPanel() {
            private val es = EntitySystem()
            private var t: Float = 0f // keeps track of the time
            private val ball = BaseEntity(es)
            private val gravity = SimpleGravity()
            private val airDrag = SimpleAirDrag(.49, Math.PI * 0.08 * 0.08) // coefficient of balls: .5, surface area: pi * r²
            private val tickTimer = Timer()
            private var currentTickTimerTask: TimerTask? = null

            init {
                ball.addComponent(LocationComponent())
                ball.addComponent(VelocityComponent())
                ball.addComponent(AccelerationComponent())
                ball.addComponent(MassComponent(.1))
                ball isSubjectTo gravity
                ball isSubjectTo airDrag

                es.register(AccelerationSystem())
                es.register(VelocitySystem())
                es.register(PhysicsMotionSystem())
                es.assureExecutionOrder(
                    AccelerationSystem::class,
                    VelocitySystem::class,
                    PhysicsMotionSystem::class
                )

                reset()
            }

            fun reset() {
                t = 0f

                ball.getComponent(VelocityComponent::class).velocity = Vector(8.0, 20.0, 0.0)
                ball.getComponent(AccelerationComponent::class).acceleration = Vector.ORIGIN
                ball.getComponent(LocationComponent::class).position = Vector(3.0, 8.0, 0.0)
            }

            fun start() {
                synchronized(tickTimer) {
                    if (currentTickTimerTask != null) {
                        currentTickTimerTask!!.cancel()
                        currentTickTimerTask = null
                    }
                    currentTickTimerTask = object : TimerTask() {
                        override fun run(): Unit = tick()
                    }
                    tickTimer.scheduleAtFixedRate(currentTickTimerTask, 0, 1000 / TICKRATE.toLong())
                }
            }

            fun tick() {
                val d = 1f / TICKRATE / 2
                t += d
                es.tick(d)

                if (t >= 4) { // stop after 30 seconds
                    currentTickTimerTask?.cancel()
                    reset()
                }
                repaint()
                println("t = $t;")
                println("a = ${ball.getComponent(AccelerationComponent::class).acceleration}")
                println("v = ${ball.getComponent(VelocityComponent::class).velocity}")
                println("s = ${ball.getComponent(LocationComponent::class).position}")
                println("---")
            }

            override fun paint(g_: Graphics) {


                val g = g_ as Graphics2D

                val actual_location = ball.getComponent(LocationComponent::class).position
                val location = mapToScreen(actual_location)

                // reset
                g.color = Color.WHITE
                g.fillRect(0, 0, width, height)

                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

                // draw the effective vectors
                fun drawVector(v: Vector) {
                    val vectorLineDestPos = mapToScreen(actual_location + v)
                    g.drawLine(location.x, location.y, vectorLineDestPos.x, vectorLineDestPos.y)
                }

                g.color = Color.MAGENTA
                drawVector(ball.getComponent(ForceSubjectComponent::class).summedForce)

                g.color = Color.BLUE
                drawVector(ball.getComponent(VelocityComponent::class).velocity)

                g.color = Color.RED
                drawVector(airDrag(ball))
                drawVector(gravity(ball))

                // draw ball
                g.color = Color.BLACK
                g.fillOval(location.x, location.y, 4, 4)
            }

            private fun mapToScreen(location: Vector): Point {
                // x width: 1000
                // y height: 1000
                return Point(
                        (20 * location.x).toInt(),
                        height - (20 * location.y).toInt()
                )
            }
        }
    }
}

fun main(args: Array<String>) {
    val frame = ThrownBallFrame()
    frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    frame.isVisible = true
}
package com.tmarsteel.jecs.physics;

import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.util.Vector

/**
 * Models acceleration of a component in units per second.
 */
public class AccelerationComponent(var acceleration: Vector = Vector.ORIGIN) : Component
{
    override val type = AccelerationComponent::class.java

    /**
     * Cache for [.adjustedAcceleration]. It is expected that the [com.tmarsteel.jecs.EntitySystem] and
     * thus the motion systems will always calculate with the same tick duration. Caching the result will speed
     * things up and safe memory.
     */
    private var lastAdjustedAcceleration: Vector = acceleration

    /**
     * The value of the parameter [.adjustedAcceleration] was last called with. If it differs from the given
     * parameter the vector will be calculated again; otherwise [.lastAdjustedAcceleration] will be returned.
     */
    private var lastAdjustedAccelerationFactor : Float = 1f

    /**
     * The object this component locks on when the adjusted velocity has to be re-calculated
     */
    private val adjustedLock = Object()

    /**
     * Returns the current acceleration of this component multiplied by `factor`. Effectively returns
     * `acceleration.multiply(factor)`. However, the result is cached since it is expected that, because
     * the tick duration is stable most of the time, `factor` changes rarely.
     * @param factor
     * @return The current velocity of this component multiplied by `factor`.
     */
    fun adjustedAcceleration(factor: Float): Vector {
        if (lastAdjustedAccelerationFactor != factor) {
            synchronized (adjustedLock) {
                if (lastAdjustedAccelerationFactor != factor) {
                    lastAdjustedAccelerationFactor = factor
                    lastAdjustedAcceleration = acceleration.multiply(factor.toDouble())
                }
            }
        }

        return lastAdjustedAcceleration
    }
}

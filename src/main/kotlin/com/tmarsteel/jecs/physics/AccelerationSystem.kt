package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.system.ComponentSystem

/**
 * Recalculates the acceleration stored in [AccelerationComponent]s based on the [ForceSubjectComponent] and
 * [MassComponent]
 */
public class AccelerationSystem : ComponentSystem
{
    override fun tick(es: EntitySystem, tickDuration: Float)
    {
        es.each({ entity ->
            val forceV = entity.getComponent(ForceSubjectComponent::class).summedForce
            val accComp = entity.getComponent(AccelerationComponent::class)
            val mass = entity.getComponent(MassComponent::class).mass

            accComp.acceleration = forceV * (1.0 / mass)
        },
            AccelerationComponent::class,
            ForceSubjectComponent::class,
            MassComponent::class
        )
    }
}
package com.tmarsteel.jecs.physics;

import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.NoSuchComponentException
import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.util.Vector
import java.util.*
import javax.naming.OperationNotSupportedException

/**
 * This component allows entities to be subject to forces pulling it in different directions. The summed forces are
 * used by the [ForceSystem] to update the entities [AccelerationComponent].
 */
public class ForceSubjectComponent(vararg initialProviders: (Entity) -> Vector) : Component
{
    override val type = ForceSubjectComponent::class.java

    private var myEntity: Entity? = null

    /**
     * The force suppliers for the given component. All the returned [Vector]s will be summed up to the resulting
     * acting force.
     */
    val forceProviders: MutableSet<(Entity) -> Vector> = HashSet()
    init {
        forceProviders.addAll(initialProviders)
    }

    /** Adds the given force provider to this component */
    operator fun plusAssign(p: (Entity) -> Vector): Unit
    {
        forceProviders.add(p)
    }

    /** Adds the given force provider to this component */
    operator fun plusAssign(f: Vector): Unit
    {
        forceProviders.add({ e -> f })
    }

    /** Removes the given force provider from this component */
    operator fun minusAssign(p: (Entity) -> Vector): Unit
    {
        forceProviders.remove(p)
    }

    override fun onRegister(e: Entity) {
        super.onRegister(e)

        if (myEntity != null) {
            throw OperationNotSupportedException("This component cannot be registered with multiple entities.")
        }

        myEntity = e
    }

    override fun onUnregister(e: Entity) {
        super.onUnregister(e)
        if (e === myEntity) {
            myEntity = null
        }
    }

    val summedForce: Vector
        get() {
            val e = myEntity ?: throw IllegalStateException("This component has not yet been registered with any entity.")

            return forceProviders.map({ p -> p(e) }).reduce(Vector::add)
        }
}


package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.component.Component

/**
 * Assigns a mass to an entity. This is Higgs boson.
 */
class MassComponent(var mass: Double) : Component {
    override val type = MassComponent::class.java
}
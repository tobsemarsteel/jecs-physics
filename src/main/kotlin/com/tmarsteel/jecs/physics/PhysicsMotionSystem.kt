package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.system.ComponentSystem
import com.tmarsteel.jecs.system.motion.LocationComponent
import com.tmarsteel.jecs.system.motion.VelocityComponent

/**
 * Simulates physics based motion. Updates the location stored in [LocationComponent] based on the acceleration stored
 * in [AccelerationComponent]. If the entity has a [VelocityComponent], that is updated, too.
 */
public class PhysicsMotionSystem : ComponentSystem {
    override fun tick(es: EntitySystem, tickDuration: Float) {
        es.each({ entity ->
            val velComp = entity.getComponent(VelocityComponent::class)
            entity.getComponent(LocationComponent::class).shiftBy(velComp.velocity * tickDuration)
        },
            LocationComponent::class,
            VelocityComponent::class
        )
    }
}

package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.system.ComponentSystem
import com.tmarsteel.jecs.system.motion.VelocityComponent

class VelocitySystem : ComponentSystem
{
    override fun tick(es: EntitySystem, tickDuration: Float) {
        es.each({ entity ->
            val acceleration = entity.getComponent(AccelerationComponent::class).acceleration
            entity.getComponent(VelocityComponent::class).velocity += acceleration * tickDuration
        },
            AccelerationComponent::class,
            VelocityComponent::class
        )
    }
}
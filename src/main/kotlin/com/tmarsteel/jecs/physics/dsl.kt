/**
 * This file contains some DSL syntax sugar
 */
package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.NoSuchComponentException
import com.tmarsteel.jecs.util.Vector

/**
 * Adds the given force provider to the [ForceSubjectComponent] of the entity.
 */
infix fun Entity.isSubjectTo(force: (Entity) -> Vector) {
    val forceSubjComp = try {
        this.getComponent(ForceSubjectComponent::class)
    }
    catch (ex: NoSuchComponentException) {
        val comp = ForceSubjectComponent()
        this.addComponent(comp)
        comp
    }

    forceSubjComp.forceProviders.add(force)
}
/**
 * This file contains various force providers (`(Entity) -> Vector`) vor various purposes.
 */
package com.tmarsteel.jecs.physics

import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.util.Vector
import com.tmarsteel.jecs.system.motion.VelocityComponent

/**
 * Returns a force provider that simulates a static gravity in -Y direction.
 * @param gravityPerMassUnit The gravity per mass unit. You can use the default if you specify mass in kilograms and
 *                           distance in meters.
 */
public fun SimpleGravity(gravityPerMassUnit: Double = 9.81): (Entity) -> Vector
{
    return { entity ->
        val mc = entity.getComponent(MassComponent::class.java)

        Vector(0.0, -1.0 * (gravityPerMassUnit * mc.mass), 0.0)
    }
}

/**
 * Returns a force provider that calculates general drag with the formular
 * [F = d * v² * c * A](https://en.wikipedia.org/wiki/Drag_equation)
 * Where
 *
 * <table>
 *     <tr>
 *         <th>Variable</th>
 *         <tg>Is</th>
 *     </tr>
 *     <tr>
 *         <td>F</td>
 *         <td>Drag force in N</td>
 *     </tr>
 *     <tr>
 *         <td>d</td>
 *         <td>density of the enclosing fluid in kg/m³</td>
 *     </tr>
 *     <tr>
 *         <td>v</td>
 *         <td>Velocity of the entity, as determined using its [VelocityComponent] in m/s</td>
 *     </tr>
 *     <tr>
 *          <td>c</td>
 *          <td>The drag coefficient</td>
 *     </tr>
 *     <tr>
 *          <td>A</td>
 *          <td>surface area of the shape in m²</td>
 *     </tr>
 * </table>
 */
public fun SimpleDrag(density: Double, coeff: Double, surfaceArea: Double): (Entity) -> Vector
{
    return { entity ->
        val v = entity.getComponent(VelocityComponent::class.java).velocity
        val vSquared = v * v.length

        vSquared * density * coeff * surfaceArea * -1.0
    }
}

/**
 * Returns a force provider that calculates air drag in 20°C air.
 * @param coeff See [SimpleDrag]
 * @param surfaceArea See [SimpleDrag]
 */
public fun SimpleAirDrag(coeff: Double, surfaceArea: Double): (Entity) -> Vector = SimpleDrag(1.204, coeff, surfaceArea)